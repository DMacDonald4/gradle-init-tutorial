# Instructions to convert project to Gradle

1. Make sure gradle is installed using the `gradle -v` command.
2. Enter the project folder. Run `gradle init`
    - Type of project: 3 (library)
    - Language: 3 (java)
    - DSL: 1 (groovy)
    - Framework: 4 (junit jupiter)
    - Project Name: (anything you like)
    - Source Package: (anything you like in lowercase)
3. Open lib/build.gradle
    - In dependencies, get rid of the lines and comments for `api` and `implementation`.
    - Replace `testRuntimeOnly 'org.junit.jupiter:junit-jupiter-engine'` with `testRuntimeOnly 'org.junit.jupiter:junit-jupiter-engine:5.6.2'` where `5.6.2` is the version of `testImplementation`, directly above.
    - Your dependencies should look like the following:
    ```gradle
    dependencies {
    // Use JUnit Jupiter API for testing.
    testImplementation 'org.junit.jupiter:junit-jupiter-api:5.6.2'

    // Use JUnit Jupiter Engine for testing.
    testRuntimeOnly 'org.junit.jupiter:junit-jupiter-engine:5.6.2'
    }
    ```
    - The other two dependencies are generated, but not needed. Adding the version to `testRuntimeOnly` allows the `.gitlab-ci.yml` file to work properly. See below for details.
    - Be sure the version for the dependencies match and use the default version that was generated for `testImplementation`.
4. Place all of your source code files in `lib/src/main/PACKAGE`. Delete the default `Library.java` file.
5. Place all of your test code files in `lib/src/test/PACKAGE`. Delete the default `LibraryTest.java` file.
6. In each of your java files, you'll need to add a package declaration. For instance, if you made your source package `com.something.hello`, you need to add the following to every java file:
    ```java
    package com.something.hello;
    ```
7. Download the `.gitlab-ci.yml` and place it in the main directory of your project. The reason for this file is so that gitlab will run your tests when you push a commit. (Notice the green checkmark on the [gitlab page](https://gitlab.com/DMacDonald4/gradle-init-tutorial))
8. You can run `gradle test` to run your tests.
9. Use this sample gradle project to compare to, if needed. I reccomend creating a new project from scratch, however, to make sure everything is up to date and unique for your project.
